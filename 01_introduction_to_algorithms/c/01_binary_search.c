#include <stdlib.h>
#include <stdio.h>

#define NUMBER_NOT_FOUND -1

int binary_search(int len, int list[len], int item);

int main(void)
{
	int my_list[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41};
	int len = sizeof(my_list) / sizeof(my_list[0]);

	printf("%d\n", binary_search(len, my_list, 3)); // 1
	printf("%d\n", binary_search(len, my_list, -1)); //-1
    
	return EXIT_SUCCESS;
}

int binary_search(int len, int list[len], int item)
{
	int low = 0;
	int high = len;

    while (low <= high) {
		int mid = (low + high) / 2;
		int guess = list[mid];

		if (guess == item) {
			return mid;
		} else if (guess > item) {
			high = mid - 1;
		} else {
			low = mid + 1;
		}

    }

    return NUMBER_NOT_FOUND; //number not found
}
