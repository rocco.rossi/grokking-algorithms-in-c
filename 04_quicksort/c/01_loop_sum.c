#include <stdlib.h>
#include <stdio.h>

int sum(int size, int arr[size]);

int main(void)
{
	int arr[] = {1, 2, 3, 4};
    int size = sizeof(arr) / sizeof(arr[0]);
    
	printf("Result: %d\n", sum(size, arr));

	return EXIT_SUCCESS;
}

int sum(int size, int arr[size])
{
	int total = 0;
    
	for (int i = 0; i < size; ++i) {
		total += arr[i];
	}
    
	return total;
}

