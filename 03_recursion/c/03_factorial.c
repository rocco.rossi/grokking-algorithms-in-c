#include <stdlib.h>
#include <stdio.h>

int fact(int x)
{

	if (x == 1) {
		return 1;
    } else {
		return x * fact(x - 1);
    }

}

int main(void)
{
	printf("Result: %d\n", fact(5));

	return EXIT_SUCCESS;
}
