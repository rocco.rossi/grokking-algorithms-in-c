#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

int find_smallest(int n, int arr[n]);
int *selection_sort(int n, int arr[n]);

int main(void)
{
	int arr[] = {23, 11, 5, 41, 37, 29, 7, 17, 31, 2, 13, 3, 19};
    int len = sizeof(arr) / sizeof(arr[0]);
	int *sort_arr = selection_sort(len, arr);

    if (sort_arr) {
        
        // print result
        for (int i = 0; i < len; ++i) {
            printf("%d ", sort_arr[i]);
        }

        printf("\n");

        free(sort_arr);
    } else {
        printf("ERROR: insufficient memory\n");
    }
    
	return EXIT_SUCCESS;
}

// Finds the smallest value in an array
int find_smallest(int n, int arr[n])
{
	// Stores the smallest value
	int smallest = arr[0];
	// Stores the index of the smallest value
	int smallest_index = 0;
    
	for (int i = 1; i < n; ++i) {

        if (arr[i] < smallest) {
			smallest = arr[i];
			smallest_index = i;
		}
        
	}
    
	return smallest_index;
}

int *selection_sort(int n, int arr[n])
{
	// Create new array
	int *new_arr = (int *) malloc(n * sizeof(int));

    if (new_arr) {

        for (int i = 0; i < n; ++i) {
            int smallest = find_smallest(n, arr);
            
            new_arr[i] = arr[smallest];
            // same as deleted by changing to the largest value
            arr[smallest] = INT_MAX;
        }

    }
    
	return new_arr;
}
